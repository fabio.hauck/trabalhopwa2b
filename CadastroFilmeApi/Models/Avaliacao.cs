using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CadastroFilmeApi.Models
{
    public class Avaliacao : BaseModel
    {
        
        
        public string FilmeId { get; set; }

        public string EmailAvaliador { get; set; }

        public string Comentario { get; set; }
        
        public double NotaAvaliacao { get; set; }

    }
}