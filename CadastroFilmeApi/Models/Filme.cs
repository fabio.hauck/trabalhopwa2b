using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CadastroFilmeApi.Models
{
    public class Filme : BaseModel
    {   
        public string Titulo { get; set; } = "";

        public string Descricao { get; set; }

        public string Classificacao { get; set; }
        


    }
}