using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CadastroFilmeApi.Models;

namespace CadastroFilmeApi.DAOs
{
    public class FilmeDAO : AutoDAO<Filme>
    {
        protected override string Tabela => "filme";
    }
}