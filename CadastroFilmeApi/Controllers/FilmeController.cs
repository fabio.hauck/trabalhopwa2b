using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CadastroFilmeApi.Models;
using CadastroFilmeApi.DAOs;

namespace CadastroFilmeApi.Controllers
{
        [ApiController]
    [Route("api/[controller]")]
    public class FilmeController : ControllerBase
    {
        // GET: api/Filme
        [HttpGet]
        public async Task<ActionResult<IList<Filme>>> Get()
        {
            return Ok(await dao.RetornarTodosAsync());
        }

        // GET: api/Filme/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Filme>> GetPorId(string id)
        {
            var objeto = await dao.RetornarPorIdAsync(id);

            if (objeto == null)
                return NotFound();
            
            return Ok(objeto);
        }

        // POST: api/Filme
        [HttpPost]
        public async Task<ActionResult<Filme>> Post(Filme objeto)
        {
            if (objeto == null)
                return Problem("O Filme que você está tentando inserir está nulo.");

            await dao.InserirAsync(objeto);

            return CreatedAtAction(nameof(GetPorId), new { id = objeto.Id }, objeto);
        }

        // PUT: api/Filme/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id, Filme novoFilme)
        {
            if (id != novoFilme.Id)
                return Problem("Como você pode me enviar um id na rota diferente do id do objeto?");
                //return BadRequest();
            
            try
            {
                await dao.AlterarAsync(novoFilme);
            }
            catch (Exception)
            {
                throw;
            }

            return NoContent();
        }

        // DELETE: api/Filme/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            await dao.ExcluirAsync(id);

            var avaliacoes = await avaliacaoDao.RetornarTodosAsync();

            // Filtra as avaliações pelo FilmeId
            var avaliacoesDoFilme = avaliacoes.Where(a => a.FilmeId == id).ToList();

            foreach (var avaliacao in avaliacoesDoFilme)
            {
                await avaliacaoDao.ExcluirAsync(avaliacao.Id);
            }

            return NoContent();
        }

          // GET: api/Filme/{id}/MediaAvaliacoes
        [HttpGet("~/api/Filme/{id}/MediaAvaliacoes")]
        public async Task<ActionResult<double>> GetMediaAvaliacoesDoFilme(string id)
        {
            // Primeiro, você precisa obter o objeto Filme correspondente ao ID fornecido.
            var filme = await filmeDao.RetornarPorIdAsync(id);

            if (filme == null)
                return NotFound("Filme não encontrado.");

            // Agora, você pode obter todas as avaliações do filme usando o objeto filme.
            var avaliacoes = await avaliacaoDao.RetornarTodosAsync();


                // Filtra as avaliações pelo FilmeId
                var avaliacoesDoFilme = avaliacoes.Where(a => a.FilmeId == id).ToList();

                // Verifica se há alguma avaliação para calcular a média
                if (avaliacoesDoFilme.Count == 0)
                    return 0;

                // Calcula a média das notas das avaliações
                var mediaAvaliacoes = avaliacoesDoFilme.Average(a => a.NotaAvaliacao);

                return Ok(mediaAvaliacoes);
       
        } 

        private FilmeDAO dao = new FilmeDAO();
        private FilmeDAO filmeDao = new FilmeDAO();
        private AvaliacaoDAO avaliacaoDao = new AvaliacaoDAO();

    }

    
}