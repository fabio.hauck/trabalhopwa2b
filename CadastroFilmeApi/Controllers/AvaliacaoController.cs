using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CadastroFilmeApi.Models;
using CadastroFilmeApi.DAOs;

namespace CadastroFilmeApi.Controllers
{
        [ApiController]
    [Route("api/[controller]")]
    public class AvaliacaoController : ControllerBase
    {
        // GET: api/Avaliacao
        [HttpGet]
        public async Task<ActionResult<IList<Avaliacao>>> Get()
        {
            return Ok(await dao.RetornarTodosAsync());
        }

        // GET: api/Avaliacao/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Avaliacao>> GetPorId(string id)
        {
            var objeto = await dao.RetornarPorIdAsync(id);

            if (objeto == null)
                return NotFound();
            
            return Ok(objeto);
        }

        // POST: api/Avaliacao
        [HttpPost]
        public async Task<ActionResult<Avaliacao>> Post(Avaliacao objeto)
        {
            if (objeto == null)
                return Problem("O Avaliacao que você está tentando inserir está nulo.");

            await dao.InserirAsync(objeto);

            return CreatedAtAction(nameof(GetPorId), new { id = objeto.Id }, objeto);
        }

        // PUT: api/Avaliacao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id, Avaliacao novoAvaliacao)
        {
            if (id != novoAvaliacao.Id)
                return Problem("Como você pode me enviar um id na rota diferente do id do objeto?");
                //return BadRequest();
            
            try
            {
                await dao.AlterarAsync(novoAvaliacao);
            }
            catch (Exception)
            {
                throw;
            }

            return NoContent();
        }

        // DELETE: api/Avaliacao/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            await dao.ExcluirAsync(id);
            
            return NoContent();
        }

        // GET: api/Filme/{id}/Avaliacoes
        [HttpGet("~/api/Filme/{id}/Avaliacoes")]
        public async Task<ActionResult<IList<Avaliacao>>> GetAvaliacoesDoFilme(string id)
        {
            // Primeiro, você precisa obter o objeto Filme correspondente ao ID fornecido.
            var filme = await filmeDao.RetornarPorIdAsync(id);

            if (filme == null)
                return NotFound("Filme não encontrado.");

            // Agora, você pode obter todas as avaliações do filme usando o objeto filme.
            var avaliacoes = await avaliacaoDao.RetornarTodosAsync();

            // Filtra as avaliações pelo FilmeId
            var avaliacoesDoFilme = avaliacoes.Where(a => a.FilmeId == id).ToList();

            return Ok(avaliacoesDoFilme);
        }

      
       
        private AvaliacaoDAO dao = new AvaliacaoDAO();
        private FilmeDAO filmeDao = new FilmeDAO();
        private AvaliacaoDAO avaliacaoDao = new AvaliacaoDAO();

    }
}